function sminc(channels, vals, bound)
% sminc(channels, vals)
%
% increment channel values

if nargin < 3 || isempty(bound)
	bound = 0.05;
end

if any(abs(vals) > abs(bound))
	error('Crayz big change. I don''t believe you');
end

vals = vals + cell2mat(smget(channels));
smset(channels, vals);
