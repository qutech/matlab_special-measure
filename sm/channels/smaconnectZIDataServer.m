function [dev_id, props] = smaconnectZIDataServer(data_server_ip, dev_id, port, apilevel, device_connection)
% connect via running data server

if nargin < 3
    port = 8004;
end

if nargin < 4
    apilevel = 5;
end

if nargin < 5
    device_connection = '1GbE';
end

% Necessary to have a designated data server running on
% a computer for multiple ZI instruments
ziDAQ('connect', data_server_ip, port, apilevel);

% Based on ziCreateAPISession:
dev_id = lower(ziDAQ('discoveryFind', dev_id));
props = ziDAQ('discoveryGet', dev_id);
try
    ziDAQ('connectDevice', dev_id, device_connection);
catch ME
    if strcmp(ME.identifier, 'ziAPI:error_32790')
        % This means data_server is running on device.
        % Then the correct interface is PCIe.
        ziDAQ('connectDevice', dev_id, 'PCIe');
    else
        rethrow(ME)
    end
end