function scan = smaconfigwrap_save_data(scan, fieldName, fn, varargin)
%function scan = smaconfigwrap(scan, fn, varargin)
% Run fn, varargin on config and deposit return argument in
% scan.data.(fieldName)

if isempty(fn)
	data = varargin;
else
	data = fn(varargin{:});
end
scan.data.(fieldName) = data;
