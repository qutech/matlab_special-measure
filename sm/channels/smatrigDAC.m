function smatrigDAC(ic) %set trigger for ramp channels

smcDecaDAC4([ic 1], 0, Inf); % trigger reset 
smcDecaDAC4([ic 1], 2.9, Inf); % max alazar trig in is +-3V, protected up to +-5V
% default trigger level in sm_setups.common.AlazarDefaultSettings 0.75*5V

%fprintf(smdata.inst(tds).data.inst, 'TRIG FORCE');
