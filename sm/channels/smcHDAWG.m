function [val, rate] = smcHDAWG(ico, val, rate)

% channels 1:8 -> offsets
% channels 9:16 -> on / off
% channels 17:24 -> marker
%   1 -> HIGH
%   0 -> LOW
%   Inf(default) -> Sequence controlled
%   -Inf -> Other (only gettable)
%   -8:-1 -> mapped to trigger input

HDAWGTriggerOutSource.HIGH = int64(17);
HDAWGTriggerOutSource.LOW = int64(18);
HDAWGTriggerOutSource.TRIG_IN_1 = int64(8);  % Trigger output assigned to trigger inout 1.
HDAWGTriggerOutSource.TRIG_IN_2 = int64(9);  % Trigger output assigned to trigger inout 2.
HDAWGTriggerOutSource.TRIG_IN_3 = int64(10);  % Trigger output assigned to trigger inout 3.
HDAWGTriggerOutSource.TRIG_IN_4 = int64(11);  % Trigger output assigned to trigger inout 4.
HDAWGTriggerOutSource.TRIG_IN_5 = int64(12);  % Trigger output assigned to trigger inout 5.
HDAWGTriggerOutSource.TRIG_IN_6 = int64(13);  % Trigger output assigned to trigger inout 6.
HDAWGTriggerOutSource.TRIG_IN_7 = int64(14);  % Trigger output assigned to trigger inout 7.
HDAWGTriggerOutSource.TRIG_IN_8 = int64(15);  % Trigger output assigned to trigger inout 8.
HDAWGTriggerOutSource.OUT_1_MARK_1 = int64(4);
HDAWGTriggerOutSource.OUT_1_MARK_2 = int64(5);

global smdata plsdata;

switch ico(3) % mode
    case 6 % single out case 6 for init mainly -> seems hacked
        switch ico(2) %channel
            case 1 %init
                % clear ziDAQ;
                
                if isfield(smdata.inst(ico(1)).data,'device_name')
                    device_id=smdata.inst(ico(1)).data.device_name;
                else
                    disp('Please create field <device_name> in inst.data')
                    %device_id='dev3331';
                    return
                end
                
                
                % Check the ziDAQ MEX (DLL) and Utility functions can be found in Matlab's path.
                if ~(exist('ziDAQ') == 3) && ~(exist('ziCreateAPISession', 'file') == 2)
                    fprintf('Failed to either find the ziDAQ mex file or ziDevices() utility.\n')
                    fprintf('Please configure your path using the ziDAQ function ziAddPath().\n')
                    fprintf('This can be found in the API subfolder of your LabOne installation.\n');
                    fprintf('On Windows this is typically:\n');
                    fprintf('C:\\Program Files\\Zurich Instruments\\LabOne\\API\\MATLAB2012\\\n');
                    return
                end
                
                apilevel=6;
                port = 8004;
                
                if isfield(smdata.inst(ico(1)).data,'data_server') && ~isempty(smdata.inst(ico(1)).data.data_server)
                    data_server_ip = smdata.inst(ico(1)).data.data_server;
                    [dev_id, props] = smaconnectZIDataServer(data_server_ip, device_id, port, apilevel);
                else
                    % Create an API session. Connect to the correct Data Server
                    % for the device.
                    [dev_id, props] = ziCreateAPISession(device_id, apilevel);
                end
                
                % Create an API session; connect to the correct Data Server for the device.
                smdata.inst(ico(1)).data.inst.device = dev_id;
                smdata.inst(ico(1)).data.inst.props = props;
                smdata.inst(ico(1)).data.inst.Status='open';
                
                if isfield(smdata.inst(ico(1)).data, 'qupulse')
                    zihdawg = py.importlib.import_module('qupulse.hardware.awgs.zihdawg');
                    smdata.inst(ico(1)).data.qupulse = zihdawg.HDAWGRepresentation(dev_id);
                    
                    % we only support this mode right now
                    smdata.inst(ico(1)).data.qupulse.channel_grouping = zihdawg.HDAWGChannelGrouping.CHAN_GROUP_1x8;
                    
                    % internal structure to configure trigger source for
                    % special measure programs
                    program_manager = py.getattr(smdata.inst(ico(1)).data.qupulse.channel_tuples{1}, '_program_manager');
                    compiler_settings = py.getattr(program_manager, '_compiler_settings');
                    compiler_settings.append({py.re.compile('_special_measure.*'), struct('trigger_wait_code', 'waitDigTrigger(1);')});
                    
                    if ~isempty(plsdata) && isstruct(plsdata) && isfield(plsdata, 'awg')
                        plsdata.awg.inst = smdata.inst(ico(1)).data.qupulse;
                    end
                end
                
                
                
                ziDAQ('sync');
                
                %Make all other important settings on device!
                %END init
                
            otherwise
                error('Operation not supported');
        end
        
    case 0 % get
        if ico(2) <= 8
            val=ziDAQ('getDouble', sprintf('/%s/sigouts/%i/offset',...
                smdata.inst(ico(1)).data.inst.device,ico(2)-1));
        elseif ico(2) <= 16
            val = ziDAQ('getInt', sprintf('/%s/sigouts/%i/on',...
                smdata.inst(ico(1)).data.inst.device,ico(2)-9));
        elseif ico(2) <= 24
            device_name = smdata.inst(ico(1)).data.inst.device;
            ch = ico(2) - 17;
            enum = ziDAQ('getInt', sprintf('/%s/triggers/out/%i/source',...
                device_name, ch));
            val = marker_enum_to_double(enum);
        else
            error('Invalid channel: %d', ico(2));
        end
        
    case 1 % set
        % Check amp mode and |val|<1.25
        if ico(2) <= 8
            if ~ziDAQ('getInt', sprintf('/%s/sigouts/%i/direct',smdata.inst(ico(1)).data.inst.device,ico(2)-1))
                
                setval = util.force_range(val,[-1.25 1.25]);
                if setval ~= val
                    warning('Offset is outside valid range!')
                    val = setval;
                end
                
                ziDAQ('setDouble', sprintf('/%s/sigouts/%i/offset',...
                    smdata.inst(ico(1)).data.inst.device,ico(2)-1),...
                    val);
                val=ziDAQ('getDouble', sprintf('/%s/sigouts/%i/offset',...
                    smdata.inst(ico(1)).data.inst.device,ico(2)-1));
                
            else
                warning('Direct mode does not allow for offset voltages')
            end
        elseif ico(2) <= 16
            ziDAQ('setInt', sprintf('/%s/sigouts/%i/on',...
                smdata.inst(ico(1)).data.inst.device,ico(2)-9),...
                val);
            val = ziDAQ('getInt', sprintf('/%s/sigouts/%i/on',...
                smdata.inst(ico(1)).data.inst.device,ico(2)-9));
        elseif ico(2) <= 24
            device_name = smdata.inst(ico(1)).data.inst.device;
            ch = ico(2) - 17;
            enum = marker_double_to_enum(val, ch);
            ziDAQ('setInt', sprintf('/%s/triggers/out/%i/source',...
                device_name, ch),...
                enum);
            
            enum = ziDAQ('getInt', sprintf('/%s/triggers/out/%i/source',...
                device_name, ch));
            val = marker_enum_to_double(enum);
        elseif ico(2) <= 32 % Rampable HDAWG channel
            ch = ico(2) - 24;
            
            if nargin > 2 && rate < 0 % set start value
                smdata.inst(ico(1)).data.program_data.ramps(ch).end_level = val;
                smdata.inst(ico(1)).data.program_data.ramps(ch).rate = -rate;
            else
                smdata.inst(ico(1)).data.program_data.ramps(ch).start_level = val;
            end
            
        else
            error('Invalid channel: %d', ico(2));
        end
    case 3 % trigger
        assert(ico(2) <= 32);
        assert(ico(2) > 24);
        %error('todo: implement force trigger');
        
    case 4 % arm / start stop the sequencer
        ziDAQ('set', smdata.inst(ico(1)).data.awgModule,...
            'awg/enable', val);
        
    case 5 % config
        
        npoints = val;
        
        if isfield(smdata.inst(ico(1)).data, 'awgModule')
            try
                ziDAQ('clear', smdata.inst(ico(1)).data.awgModule);
            catch
                % we dont care if it was an invalid handle
            end
        end
        
        % requires we are in 1x8 mode TODO: check
        smdata.inst(ico(1)).data.awgModule = ziDAQ('awgModule');
        
        % stop eventually running sequencer
        ziDAQ('set', smdata.inst(ico(1)).data.awgModule,...
            'awg/enable', 0);
        
        % init module
        awg = smdata.inst(ico(1)).data.awgModule;
        ziDAQ('set', awg, 'device', smdata.inst(7).data.inst.device);
        ziDAQ('set', awg, 'index', 0);
        ziDAQ('execute', awg);
        
        % set / get awg settings (sample rate etc.)
        program_data = apply_settings(smdata.inst(7).data.inst.device,...
            smdata.inst(ico(1)).data.program_data);
        
        seqc = generate_seqc(program_data, npoints);
        compile_and_upload_seqc(awg, seqc);
        
        ziDAQ('sync');
        
        % forget the set values
        % so future ramps wont ramp old channels
        smdata.inst(ico(1)).data.program_data.ramps = ...
            rmfield(smdata.inst(ico(1)).data.program_data.ramps, 'end_level');
        
        
    otherwise
        error('Operation not supported.');
end

    function enum = marker_double_to_enum(val, ch)
        if val == 0
            enum = HDAWGTriggerOutSource.LOW;
        elseif 1 <= val && val <= 5
            enum = HDAWGTriggerOutSource.HIGH;
        elseif any(val == -8:-1)
            enum = (-1 - val) + HDAWGTriggerOutSource.TRIG_IN_1;
        elseif isinf(val)
            if rem(ch, 2) == 0
                enum = HDAWGTriggerOutSource.OUT_1_MARK_1;
            else
                enum = HDAWGTriggerOutSource.OUT_1_MARK_2;
            end
        else
            error('Invalid marker double value (0, 1 <= val <= 5 , Inf) %f', val);
        end
    end


    function val = marker_enum_to_double(enum)
        switch enum
            case HDAWGTriggerOutSource.LOW
                val = 0;
            case HDAWGTriggerOutSource.HIGH
                val = 1;
            case HDAWGTriggerOutSource.OUT_1_MARK_1
                val = Inf;
            case HDAWGTriggerOutSource.OUT_1_MARK_2
                val = Inf;
            case num2cell(HDAWGTriggerOutSource.TRIG_IN_1:HDAWGTriggerOutSource.TRIG_IN_8)
                val = double(-1 - (enum - HDAWGTriggerOutSource.TRIG_IN_1));
            otherwise
                val = -Inf;
        end
    end

end

function program_data = apply_settings(device, program_data)
% this function sets and re-reads the base sample rate and the exponent of
% the awg factor (sample_rate = base_rate / 2 ^ exponent)

if isfield(program_data, 'base_sample_rate')
    ziDAQ('setDouble', sprintf('/%s/system/clocks/sampleclock/freq', device), program_data.base_sample_rate);
end

if isfield(program_data, 'dig_trig_source')
    for ii_trig = 1:numel(program_data.dig_trig)
        ziDAQ('setInt', sprintf('/%s/awgs/0/auxtriggers/%d/channel', device, ii_trig - 1), program_data.dig_trig(ii_trig));
    end
end

if isfield(program_data, 'awg_sample_rate_exponent')
    ziDAQ('setInt', sprintf('/%s/awgs/0/time', device), program_data.awg_sample_rate_exponent);
end

if ~isfield(program_data, 'dig_trig_sel')
    % default digital trigger
    program_data.dig_trig_sel = 1;
end

ziDAQ('sync');
program_data.base_sample_rate = ziDAQ('getDouble', sprintf('/%s/system/clocks/sampleclock/freq', device));
program_data.awg_sample_rate_exponent = ziDAQ('getDouble', sprintf('/%s/awgs/0/time', device));

end

function seqc = generate_seqc(program_data, npoints)

sample_rate = program_data.base_sample_rate / 2^program_data.awg_sample_rate_exponent;

wf_list = cell(1, 8);

seqc = cell(1, 10);


% calculate the total time of all ramps
rates = {program_data.ramps.rate};
start_levels = {program_data.ramps.start_level};
end_levels = {program_data.ramps.end_level};

undefined = cellfun(@isempty, rates) | cellfun(@isempty, start_levels) | cellfun(@isempty, end_levels);

% convert to numeric arrays with nans
rates(undefined) = {nan};
rates = abs(cell2mat(rates));
start_levels(undefined) = {nan};
start_levels = cell2mat(start_levels);
end_levels(undefined) = {nan};
end_levels = cell2mat(end_levels);

ramp_times = abs(end_levels - start_levels) ./ rates;
ramp_samples = ceil(sample_rate * ramp_times);
total_samples = max(ramp_samples);

total_samples = total_samples + total_samples / (npoints - 1);

% extend to multiple of 16 samples (easy to read!!!)
total_samples = total_samples + mod(16 - mod(total_samples, 16), 16);

idle_samples = total_samples - ramp_samples;

for ii = 1:8
    clear ch_wf
    if undefined(ii)
        if isnan(start_levels(ii)) || start_levels(ii) == 0
            ch_wf = sprintf('zeros(%d)', total_samples);
        else
            ch_wf = sprintf('%f * ones(%d)', start_levels(ii), total_samples);
        end
    else
        ch_wf = sprintf('ramp(%d, %f, %f)',...
            ramp_samples(ii), start_levels(ii), end_levels(ii));
        if idle_samples(ii) > 0
            ch_wf = sprintf('join(%s, %f * ones(%d))',...
                ch_wf, end_levels(ii), idle_samples(ii));
        end
    end
    
    seqc{ii} = sprintf('wave wf%d = %s;', ii, ch_wf);
    wf_list{ii} = sprintf('wf%d', ii);
end
seqc{9}
% actual runtime code
seqc{9} = 'while (true) {';
seqc{10} = sprintf('\twaitDigTrigger(%d);', program_data.dig_trig_sel);
seqc{11} = sprintf('\tplayWave(%s);', strjoin(wf_list, ', '));
seqc{12} = '}';

seqc = strjoin(seqc, '\n');
end

function compile_and_upload_seqc(awg, seqc)
ziDAQ('set', awg, 'compiler/sourcestring', seqc);

while true
    status_code = ziDAQ('getDouble', awg, 'compiler/status');
    switch status_code
        case -1
            % TODO log we are compiling
            pause(.1);
        case 0
            break;
        case 1
            message = ziDAQ('getString', awg, 'compiler/statusstring', seqc);
            fprintf(2, 'Program failed to compile:\n');
            fprintf(2, seqc);
            error(message);
        case 2
            message = ziDAQ('getString', awg, 'compiler/statusstring', seqc);
            fprintf(2, 'Compilation successful with warnings:\n');
            fprintf(2, seqc);
            fprintf(2, '\n\n');
            warning(message);
            break;
        otherwise
            error('invalid status code: %d', status_code);
    end
end

while ziDAQ('getDouble', awg, 'progress') < 1.
    pause(.1);
end
end
