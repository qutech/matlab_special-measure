function val = smcQDAC(ico, val, rate)
% Experimental driver for the QDevil Q302 QDAC 24 Channel Precision DAC
% written by Eugen Kammerloher (kammerloher@physik.rwth-aachen.de) 2019.
% 	SMCQDAC([ico(1) ico(2) ico(3)] val, rate)
% 		ico(1): instrument number in rack
% 		ico(2): channel on instrument 1,3,5,...,47 = Ramp channels 1-24
%                                     2,4,6,...,48 = DC step channels 1-24
%                                     49,50,51,...,72  = Current chan 1-24
% 		ico(3): 0=read, 1=write(, 3=trig, 4=bufferreset, 5=sweepsetup, 6=init)
%
%   Even channels till 48 are set instantanuously and odd channels are
%   ramped.

% TODO
% - Switch voltage range with a smdata.inst.(ico(1).data.rng variable
%   -10V ... 10 V and -1V ... 1V possible
% - Switch current range with a smdata.inst.(ico(1).data.currrng variable
%   100 muA and 1 muA possible
% - Implement triggering with a smdata.inst.(ico(1).data.trigmode variable
%   Only output triggers possible on sync channels
% - Implement parallel ramps
% - AWG, sine output, ...

% NOTE
% Device always returns output even in non-verbose mode. At least '\n'. So
% use query and discard output, if not required.
% When a read command is missed the QDAC stores the message and replies on
% the next command with the previous message. This is frustrating, since
% BytesAvailable of the serial object does not reflect this stored message
% and I could not find a way to flush it. Only a restart seems to help.
%
% Reference driver: https://github.com/QCoDeS/Qcodes/blob/master/qcodes/instrument_drivers/QDev/QDac.py

% Commands from manual:
% adc - binary AD channel current data (200 ms delay)
% awg - set user defined waveform
% brt - change baudrate
% cal - route channel to cal output
% cur - set or read channel current range
% dac - set ot read binary DA voltage data
% fun - set or read function generator configuration 1-8
% get - read channel current (200 ms delay)
% ical - set or read current calibration constant
% pul - set or read pulse train configuration
% rang - read min and max calibrated channel output values
% run - output awg
% savecalib5599 - stores calibration parameters
% set - set or read channel DC voltage
% sernum - read serial number
% ssy - blocks till next period of generator
% status - read software version channel voltages, voltage & current ranges
% syn - set or read SYNC output configuration
% tem - read board temperature
% vcal - set or read voltage calibration constants
% ver - switch verbose mode
% vol - set or read channel voltage range
% wav - set or read which generator connected to channel

global smdata;
MAX_CHAN_NO = 24; % Maximum number of channels.

switch ico(3) % Switch on operation.
  case 0 % Read.
    if ico(2) <= MAX_CHAN_NO*2 % Voltage channels.
      chan = ceil(ico(2)/2);
      assert(chan >= 1 && chan <= MAX_CHAN_NO, 'Not a valid channel.');
      check_device_serial(ico(1)); % Correct device and in sync?
      response = smquery(ico(1), sprintf('set %d', chan));
      % Example response: 'Channel:   1   -0.000008'
      mexp = 'Channel:\s+(?<channel>\d+)\s+(?<value>[+-]?\d+\.?\d*)';
      response = regexp(response, mexp, 'names');
      assert(str2double(response.channel) == chan, ...
        'Response mismatch');
      val = str2double(response.value);
      
    else % Current channels.
      chan = ico(2) - MAX_CHAN_NO*2;
      assert(chan >= 1 && chan <= MAX_CHAN_NO, 'Not a valid channel.');
      check_device_serial(ico(1)); % Correct device and in sync?
      response = smquery(ico(1), sprintf('get %d', chan));
      % Example response: 'Channel 1 current:  -0.102491 uA'
      mexp = 'Channel\s+(?<channel>\d+)\s+current:\s+(?<value>[+-]?\d+\.?\d*)\s+uA';
      response = regexp(response, mexp, 'names');
      assert(str2double(response.channel) == chan, ...
        'Response mismatch');
      val = str2double(response.value); % This value is in uA.
      val = val*1e-6;
    end
    
  case 1 % Write.
    if ico(2) > MAX_CHAN_NO*2
      error('Current channel is read-only.');
    end
    chan = ceil(ico(2)/2);
    assert(chan >= 1 && chan <= MAX_CHAN_NO, 'Not a valid channel.');
    
    if mod(ico(2), 2) % Odd channel -> ramped channel.
      start_val = smcQDAC([ico(1:2) 0]); % could skip if same as setval for speed improvement
      
      rtime = abs(val-start_val)/abs(rate);
      if rtime <= .002
        warning('Not safe to have ramp times < .002. Set to .003');
        rtime = .003;
      end
      
      % Just always use generator 1 in blocking mode.
      % This should be heavily optimized to account for parallel
      % ramps. In principle all channel can be ramped simultanuously
      % with up to 8 different ramprates.
      % This call is blocking. The generator will be cleared
      % afterwards, so it does not ramp the attached channel
      % unintentionally later on, potentially with a discontinuity!.
      generator = 1;
      offset = start_val;
      amplitude = val-start_val;
      waveform = 3; % 3: triangle/ramp.
      period = rtime*1e3; % In ms.
      dutycycle = 100; % Falling ramp for 0 and increasing for 100.
      repetitions = 1;
      check_device_serial(ico(1)); % Correct device and in sync?
      smquery(ico(1), sprintf('wav %d %d %g %g', chan, ...
        generator, ...
        amplitude, ...
        offset));
      if rate > 0
        smquery(ico(1), sprintf('fun %d %d %g %d %d', generator, ...
          waveform, ...
          period, ...
          dutycycle, ...
          repetitions));
        pause(rtime);
        % Disconnect generator. Final amplitude and offset values are
        % converted automatically to the correct DC value.
        smquery(ico(1), sprintf('wav %d 0 0 0', chan));
      else % assume trigger comes later
        % save info for triggering
        smdata.inst(ico(1)).data.programmedRamp = struct('chan',chan,...
          'generator',generator,...
          'amplitude',amplitude,...
          'offset',offset,...
          'waveform',waveform,...
          'period',period,...
          'dutycycle',dutycycle,...
          'repetitions',repetitions);
      end
      
    else % Even channel -> step channel.
      check_device_serial(ico(1)); % Correct device and in sync?
      % Disconnect generator. Final amplitude and offset values are
      % converted automatically to the correct DC value.
      smquery(ico(1), sprintf('wav %d 0 0 0', chan));
      smquery(ico(1), sprintf('set %d %g', chan, val));
    end
  case 3
    % Only way I see right now to do this.. should definetly time this in
    % the future!
    smquery(ico(1), sprintf('syn %d %g', 1, ...
      smdata.inst(ico(1)).data.programmedRamp.generator,0,1));
    smquery(ico(1), sprintf('fun %d %d %g %d %d', ...
      smdata.inst(ico(1)).data.programmedRamp.generator, ...
      smdata.inst(ico(1)).data.programmedRamp.waveform, ...
      smdata.inst(ico(1)).data.programmedRamp.period, ...
      smdata.inst(ico(1)).data.programmedRamp.dutycycle, ...
      smdata.inst(ico(1)).data.programmedRamp.repetitions));
    % This does not reset the generator, check if this is dangerous!
    smdata.inst(ico(1)).data.programmedRamp = [];
    
  case 6 % Initialise the DAC.
    % Expect instrument to be in verbose mode. Default at start-up,
    % but make it explicit.
    smquery(ico(1), 'ver 1');
    % Remember device regardless of COM port change. Also used to check
    % synchronicity of command response.
    if isempty(smdata.inst(ico(1)).data.serial)
      smdata.inst(ico(1)).data.serial = smquery(ico(1), 'sernum');
    end
    
  otherwise
    error('Operation not supported');
end

  function check_device_serial(instNum)
    serial = smquery(instNum, 'sernum');
    assert(isequal(smdata.inst(instNum).data.serial,serial), ...
      ['Initial and current serial number do not match. ', ...
      'Wrong COM port or communication command-response ', ...
      'cycle out of sync: A stored previous response is ', ...
      'output for a new query. ', ...
      'Only a device restart helps in this case.']);
  end
end