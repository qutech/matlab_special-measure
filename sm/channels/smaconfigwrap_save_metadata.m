function metadata = smaconfigwrap_save_metadata(metadata, fieldName, fn, varargin)
%function metadata = smaconfigwrap(metadata, fieldName, fn, varargin)
% Run fn, varargin on config and deposit return argument in
% metadata.(fieldName)

if isempty(fn)
	fieldValue = varargin;
else
	fieldValue = fn(varargin{:});
end

metadata.(fieldName) = fieldValue;
