function [val, rate] = smcTabor2184C(ico, val, rate)
% [val, rate] = smcTabor2184C(ico, val, rate)
% Set/get offsets of 4 channels of Tabor AWG
% Operation 0 = get, 1 = set

% |offset*2| + |amplitude| value can not exceed 4 Vpp if in HV mode

global smdata plsdata;
tawg = smdata.inst(ico(1)).data.tawg;

if ~isempty(plsdata) && isstruct(plsdata) && isfield(plsdata, 'awg') && isfield(plsdata.awg, 'dcMode')
	dcMode = plsdata.awg.dcMode;
else
	dcMode = false;
end

switch ico(3)
	
	case 0 % get
		if ico(2) <= 4
			% val = tawg.offset(uint64(ico(2))); % HV, same as below
      tawg.send_cmd(sprintf(':INST:SEL %i', ico(2)));
			val = double(py.float(tawg.send_query(':VOLT:OFFS?'))); % HV and DC
			
		elseif ico(2) <= 8 % for getting RFA_ON through RFD_ON	
			ii = uint64(ico(2)-4);
			tawg.send_cmd(sprintf(':INST:SEL %i', ii));
			outp = char(tawg.send_query(':OUTP?'));
			val = ~strcmp(outp, 'OFF');		
			
		elseif ico(2) <= 12 % for getting MA through MD - does not seem to work in ASEQ mode
			ii = uint64(ico(2)-8);
			tawg.send_cmd(sprintf(':INST:SEL %i', ii));
			tawg.send_cmd(sprintf(':MARK:SEL %i', mod(ii-1, 2)+1));
			outp = char(tawg.send_query(':MARK:STAT?'));
			val = ~strcmp(outp, 'OFF');
			
		end

	case 1 % set
		
		if ico(2) <= 4 % for setting RFA through RFD
			tawg.send_cmd(sprintf(':INST:SEL %i', ico(2)));
			if dcMode
				amplitude = double(py.float(tawg.send_query(':SOUR:VOLT:LEV:AMPL:DC?')));
			else
				amplitude = double(py.float(tawg.send_query(':VOLT:HV?')));
			end
			
			if abs(val*2) + abs(amplitude) > 4
				warning('Offset %i not set since |offset*2 (%.3f V)| + |amplitude (%.3f V)| value can not exceed 4 Vpp if in HV mode.', ico(2), val*2, amplitude);
			end
			
			tawg.send_cmd(sprintf(':VOLT:OFFS %f', val)); % HV and DC
			
			
		elseif ico(2) <= 8 % for setting RFA_ON through RFD_ON	
			warning('Setting RFA_ON through RFD_ON not implemented yet');
			
		elseif ico(2) <= 12 % for setting MA through MD - does not seem to work in ASEQ mode
			ii = uint64(ico(2)-8);
			tawg.send_cmd(sprintf(':INST:SEL %i', ii));
			tawg.send_cmd(sprintf(':MARK:SEL %i', mod(ii-1, 2)+1));
			tawg.send_cmd(sprintf(':MARK:STAT %i', val)); 
			
		end
	otherwise
		error('Operation not supported');
		
end


