function [val, rate] = smcAtsPython(ico, val, rate, varargin)
global smdata;

%config field
%smdata.inst(ico(1)).data.config

NUM_HW_CHANNELS = 4;
VIRTUAL_CHANNEL = 9;
% OPERATION SWITCH
switch ico(3)
  
  %--------------------------------%
  % OPERATION 0: Get channel value %
  %--------------------------------%
  case 0
      if ico(2) <= NUM_HW_CHANNELS % channel one through 4 are single point acquisition (measuring multiple channels at one would require a rewrite)
          val = ATSPythonSinglePoint(ico(1), ico(2));
					
      elseif ico(2) == VIRTUAL_CHANNEL % virtual channel working together with qctoolkit and qc.daq_operations
				if ~isfield(smdata.inst(ico(1)).data, 'virtual_channel')
					error('Field ''virtual_channel'' in smdata.inst(ico(1)).data does not exist for an unknown reason. Cannot use virtual channel. Try and run qc.cad_operations(''add'').');
				end					
				
				data = smdata.inst(ico(1)).data.python_card_wrapper.extractNextScanline();				
				data = util.py.py2mat( data.operationResults);
				operations = smdata.inst(ico(1)).data.virtual_channel.operations;				
				
				for operationsInd = 1:numel(operations)					
					operationName = operations{operationsInd}{2};
					val{operationsInd} = util.py.py2mat(data.(operationName).getAsVoltage(py.getattr(py.atsaverage.alazar.InputRangeID, 'range_1_V')));					
				end				
				val = cell2mat(val);
				
			else
          channel = ico(2) - NUM_HW_CHANNELS; % 4 is the number of single point channels
          device = smdata.inst(ico(1)).data.python_card_wrapper;
          val = python_to_MATLAB( device.MATLAB_getOperationResult( uint32(channel) ) );
          if isstruct(val)
              if isfield(val, 'bins')
                  val = val.bins; % discard bins
              end
          end
      end

  %---------------------------------------%
  % OPERATION 1: Set channel value to val %
  %---------------------------------------%
  case 1
      error('Set channel capabilities have been removed.');
    
  %---------------------------------------------------%  
  % OPERATION 3: force trigger if not externally done %
  %---------------------------------------------------%
  case 3
    device = smdata.inst(ico(1)).data.python_card_wrapper;
    pause(0.05);
    device.forceTrigger();
    disp('AlazarCard: triggered');
    
  %------------------------------------------------%
  % OPERATION 4: Start/Re-arm acquisition          %
  %------------------------------------------------%
  case 4
      device = smdata.inst(ico(1)).data.python_card_wrapper;
      device.MATLAB_getOperationResult( uint32(0) ); % clear last scanline before acquisition
      device.startAcquisition(uint32(1)); % TODO: read value from config (fix)
       
  %------------------------------%
  % OPERATION 5: configure board %
  %------------------------------%
  case 5
%     % single point acquisition hack, happens only at configuration, so time
%     % is not an issue
%     st = dbtrace;
%     if ~strcomp(st(1).name, 'ATSPythonSinglePoint')
%         smdata.inst(ico(1)).singlePointConfigured = 0;
%     end
    
    smdata.inst(ico(1)).datadim =  [ones(1, NUM_HW_CHANNELS)'; zeros(length( smdata.inst(ico(1)).data.config.operations ), 1)];
    
    for op_i = 1:length(smdata.inst(ico(1)).data.config.operations)
        mask = smdata.inst(ico(1)).data.config.masks{ smdata.inst(ico(1)).data.config.operations{op_i}.mask };
        
        switch smdata.inst(ico(1)).data.config.operations{op_i}.type
            case 'DS'
                switch mask.type
                    case 'Periodic Mask'
                        smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = smdata.inst(ico(1)).data.config.total_record_size / mask.period; % 4 is for single point channels
                    case 'Auto Mask'
                        smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = length(mask.begin); % 4 is for single point channels
                    otherwise
                        error('Unknown mask: %s',mask.type);
                end
            case 'REP AV'
                switch mask.type
                    case 'Periodic Mask'
                        smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = mask.end-mask.begin;
                    case 'Auto Mask'
                        smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = mask.length(1);
                    otherwise
                        error('Unknown mask: %s',mask.type);
                end
            case 'HIST'
                smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = smdata.inst(ico(1)).data.config.operations{op_i}.bin_count;
            case 'DS + REP AV'
                smdata.inst(ico(1)).datadim(op_i + NUM_HW_CHANNELS) = smdata.inst(ico(1)).data.config.operations{op_i}.period;
            otherwise
                error('Unknown operation: %s',smdata.inst(ico(1)).data.config.operations{op_i}.type);
        end
    end
    
    device = smdata.inst(ico(1)).data.python_card_wrapper;
    parsed = smdata.inst(ico(1)).data.py.modules.config.ScanlineConfiguration.parse(smdata.inst(ico(1)).data.config);
    device.applyConfiguration(parsed, true);
    
    case 6
        if ~isfield(smdata.inst(ico(1)).data, 'log_path')
            smdata.inst(ico(1)).data.log_path = 'C:\Users\Public\AtsAverage';
        end
        if ~isfield(smdata.inst(ico(1)).data, 'hmac_key')
            smdata.inst(ico(1)).data.hmac_key = 'ultra_save_default_key';
        end
        
        % enable logging of pyro library
        util.mkdir_exist(smdata.inst(ico(1)).data.log_path);
        py.logging.basicConfig(pyargs('filename', fullfile(smdata.inst(ico(1)).data.log_path, 'pyro.log')));
        py.logging.getLogger('Pyro4').setLevel(py.logging.WARNING);
        py.logging.getLogger('Pyro4.core').setLevel(py.logging.WARNING);
        
        if strcmp(smdata.inst(ico(1)).data.address,'local')
            % (re-)load modules
            smdata.inst(ico(1)).data.py.modules.config = py.importlib.reload(py.importlib.import_module('atsaverage.config'));
            smdata.inst(ico(1)).data.py.modules.core = py.importlib.reload(py.importlib.import_module('atsaverage.core'));
            smdata.inst(ico(1)).data.py.modules.atsaverage = py.importlib.reload(py.importlib.import_module('atsaverage.atsaverage'));
            smdata.inst(ico(1)).data.py.modules.alazar = py.importlib.reload(py.importlib.import_module('atsaverage.alazar'));
            smdata.inst(ico(1)).data.py.modules.server = py.importlib.reload(py.importlib.import_module('atsaverage.server'));
            
            
            smdata.inst(ico(1)).data.py.card = smdata.inst(ico(1)).data.py.modules.core.getLocalCard(1,1);
            smdata.inst(ico(1)).data.py.card.triggerTimeout = int64(10000); % units is milliseconds
            smdata.inst(ico(1)).data.py.card.computationTimeout = int64(20000); % units is milliseconds
            smdata.inst(ico(1)).data.py.card.acquisitionTimeout = int64(20000); % units is milliseconds
            
            % start a server that listens on local host so you can access
            % the card from python.
            smdata.inst(ico(1)).data.py.server = smdata.inst(ico(1)).data.py.modules.server.Server();
            smdata.inst(ico(1)).data.py.server.start(smdata.inst(ico(1)).data.hmac_key, pyargs('hostname','localhost'));
            smdata.inst(ico(1)).data.py.modules.atsaverage.MATLABLogger.set_log_path(smdata.inst(ico(1)).data.log_path);
            smdata.inst(ico(1)).data.py.modules.atsaverage.MATLABLogger.rotate_file();
        
				elseif strcmp(smdata.inst(ico(1)).data.address, 'simulator')					
					  warning('Simulator not implemented yet');
					  smdata.inst(ico(1)).data.python_card = [];		
				
				else
            % if adress is not 'local' it is assumed to be a struct with
            % atsaverage.client.getNetworkCard argument names as fields
            
            % (re-)load modules
            smdata.inst(ico(1)).data.py.modules.client = py.importlib.reload(py.importlib.import_module('atsaverage.client'));
            smdata.inst(ico(1)).data.py.modules.config = py.importlib.reload(py.importlib.import_module('atsaverage.config'));
            
            % cell array of the names of all argument the function takes
            argument_names = python_to_MATLAB(py.list(py.inspect.signature(py.getattr(smdata.inst(ico(1)).data.py.modules.client, 'getNetworkCard')).parameters.keys()));
            
            % fill in arguments that are given by data.address
            % ignore the others
            arguments = {};
            for arg = argument_names
                if isfield(smdata.inst(ico(1)).data.address, arg{1})
                    arguments = [arguments arg {smdata.inst(ico(1)).data.address.(arg{1})}];
                end
            end
            smdata.inst(ico(1)).data.py.card = smdata.inst(ico(1)).data.py.modules.client.getNetworkCard(pyargs(arguments{:}));
        end
        smdata.inst(ico(1)).data.python_card_wrapper = atsaverage.CardWrapper(smdata.inst(ico(1)).data.py.card);
        
    case 99
        smdata.inst(ico(1)).data.config = varargin{1};
				
	  case 999 % dynamically set datadim for virtual channel 9
			smdata.inst(ico(1)).datadim(VIRTUAL_CHANNEL) = sum(val(:));
			
  otherwise
    error('Operation not supported: %i', ico(3));
end
end
