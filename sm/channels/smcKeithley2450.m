function [val, rate] = smcKeithley2450(ico, val, rate)
% driver for Keithley 2450 SourceMeter

% 	SMCKEITHLEY2450([ico(1) ico(2) ico(3)] val, rate)
% 		ico(1): instrument number in rack
% 		ico(2): channel on instrument 1 =VOLT
%                                     2 =CURRENT, 
%									  3 =COMPLIANCE
%                                     4 =ISSOURCEVOLT
%                                     5 =OUTPUTON
%                                     6 =4PTMEAS
% 		ico(3): 0=read, 1=write(, 3=trig, 4=bufferreset, 5=sweepsetup)

% measurement range is determined by instrument
% no ramps/triggering implimented, only dc voltage/current measure/source
% written by sebastian.kindel@rwth-aachen.de

global smdata;

switch ico(2)
	case 1	% measure voltage or set constant voltage source
		switch ico(3)
			case 0
				fprintf(smdata.inst(ico(1)).data.inst, ':SENS:VOLT:RANG:AUTO ON');
                val = query(smdata.inst(ico(1)).data.inst, ':MEAS:VOLT?', '%s\n', '%f');                

			case 1
				fprintf(smdata.inst(ico(1)).data.inst, ':SOUR:FUNC VOLT');
				fprintf(smdata.inst(ico(1)).data.inst, ':SOUR:VOLT:LEV %f', val);

			otherwise
				error('Operation not supported');
		end

	case 2	% measure current or set constant current on source
		switch ico(3)
			case 0
				fprintf(smdata.inst(ico(1)).data.inst, ':SENS:CURR:RANG:AUTO ON');
                val = query(smdata.inst(ico(1)).data.inst, ':MEAS:CURR?', '%s\n', '%f');        

			case 1
				fprintf(smdata.inst(ico(1)).data.inst, ':SOUR:FUNC CURR');
				fprintf(smdata.inst(ico(1)).data.inst, ':SOUR:CURR:LEV %f', val);

			otherwise
				error('Operation not supported');
		end

	case 3 % get real compliance as minimum of measuring range and compliance value. Set compliance
		switch ico(3)
			case 0
				source = query(smdata.inst(ico(1)).data.inst,  ':SOUR:FUNC?', '%s\n', '%s');
                if strcmp(source, 'VOLT')
					sense = 'CURR';
				else
					sense = 'VOLT';
                end
                
				% Query measurement range, compliance limit
				range = query(smdata.inst(ico(1)).data.inst,  [':SOUR:' sense ':RANGE?'], '%s\n', '%f');
                if strcmp(source, 'VOLT')
					sense = 'CURR';
                    prot = query(smdata.inst(ico(1)).data.inst, [':SOUR:' sense ':VLIM?']);
				else
					sense = 'VOLT';
                    prot = query(smdata.inst(ico(1)).data.inst, [':SOUR:' sense ':ILIM?']);
                end
                
				% Take the smaller one as compliance readback
				val = min(range, str2num(prot));

			case 1 % does not work for ohm
				source = query(smdata.inst(ico(1)).data.inst,  ':SOUR:FUNC?', '%s\n', '%s');
                fprintf(smdata.inst(ico(1)).data.inst, [':SOUR:' source ':RANG:AUTO ON']);
                if strcmp(source, 'VOLT')
					sense = 'CURR';
                    fprintf(smdata.inst(ico(1)).data.inst, [':SOUR:' sense ':VLIM %f'], val);
				else
					sense = 'VOLT';
                    fprintf(smdata.inst(ico(1)).data.inst, [':SOUR:' sense ':ILIM %f'], val);
                end

			otherwise
				error('Operation not supported');
		end

	case 4 % get 1 if sourcing voltage. Set 1 to source voltage (0 for current)
		switch ico(3)
			case 0 % instrument outputs VOLT, CURR
				val = query(smdata.inst(ico(1)).data.inst,  ':SOUR:FUNC?', '%s\n', '%s');
                if strcmp(val, 'VOLT')
					val = 1;
				else
					val = 0;
                end

			case 1
                if val == 1
					cmd = 'VOLT';
				else
					cmd = 'CURR';
                end
				fprintf(smdata.inst(ico(1)).data.inst, ':SOUR:FUNC %s', cmd);

			otherwise
				error('Operation not supported');
		end

	case 5 % get 1 if output is on. Set 1 to set output on (0 for off)
		switch ico(3)
			case 0 % instruments outputs 1 for ON, 0 for OFF
				val = query(smdata.inst(ico(1)).data.inst,  ':OUTP:STAT?', '%s\n', '%d');
                
			case 1
                if (val == 1)
					cmd = 'ON';
				else
					cmd = 'OFF';
                end
				fprintf(smdata.inst(ico(1)).data.inst, ':OUTP:STAT %s', cmd);

			otherwise
				error('Operation not supported');
        end
        
    case 6 % get 1 if 4wiresense is on. Set 1 to set output on (0 for off)
		switch ico(3)
			case 0 % instruments outputs 1 for ON, 0 for OFF
                
				val = query(smdata.inst(ico(1)).data.inst,  ':SOUR:FUNC?', '%s\n', '%s');
                
                if val ==1; % Source is VOLT, Sense is CURR                    
                    val = query(smdata.inst(ico(1)).data.inst,  ':SENS:CURR:RSEN?', '%s\n', '%d');
                else % Source is CURR, Sense is VOLT                    
                    val = query(smdata.inst(ico(1)).data.inst,  ':SENS:VOLT:RSEN?', '%s\n', '%d');
                end
                
			case 1
                if (val == 1)
					cmd = 'ON';
				else
					cmd = 'OFF';
                end
                
                fprintf(smdata.inst(ico(1)).data.inst, ':SENS:CURR:RSEN %s', cmd);
                fprintf(smdata.inst(ico(1)).data.inst, ':SENS:VOLT:RSEN %s', cmd);

			otherwise
				error('Operation not supported');
        end
       
	otherwise
		%error('Operation not supported');
		error(['Channel ', num2str(ico(2)) ,' is not available']);
end